import requests
import json
import urllib3

url = "https://192.168.122.100/api/dcim/devices"

payload={}

headers = {
  'Content-Type': 'application/json',
  'Authorization': 'Token 2ab2c784426e441aac745966d81319efc01dee0e'
}

response = requests.request("GET", url, headers=headers, data=payload, verify=False)
json_data = response.json()
results = json_data['results']

for device in results:
  hostname = device['name']
  ipaddr = device['primary_ip']['address']
  print(f"The IP address of " + hostname + ' is ' + ipaddr + '.')